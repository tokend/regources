/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package regources

type LpSwapOpAttributes struct {
	InAmount        Amount `json:"in_amount"`
	LiquidityPoolId int32  `json:"liquidity_pool_id"`
	OutAmount       Amount `json:"out_amount"`
	SwapType        string `json:"swap_type"`
}
