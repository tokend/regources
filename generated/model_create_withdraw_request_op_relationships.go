/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package regources

type CreateWithdrawRequestOpRelationships struct {
	Balance *Relation `json:"balance,omitempty"`
	Request *Relation `json:"request,omitempty"`
}
