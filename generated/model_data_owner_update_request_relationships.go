/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package regources

type DataOwnerUpdateRequestRelationships struct {
	Data     *Relation `json:"data,omitempty"`
	NewOwner *Relation `json:"new_owner,omitempty"`
}
